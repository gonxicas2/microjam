﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickable : MonoBehaviour
{
    [SerializeField] private int value;
    [SerializeField] private float weight;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            AudioManager.Instance.PlayTakeThingSound();
            GiveObject.Instance.values.Add(value);
            GiveObject.Instance.weights.Add(weight);
            GiveObject.Instance.ResfreshTotalWeight();
            Destroy(gameObject);
        }
    }
}
