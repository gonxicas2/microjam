using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiveObject : Singleton<GiveObject>
{
    [HideInInspector] public List<int> values;
    [HideInInspector] public List<float> weights;
    [HideInInspector] public float totalWeights = 0;

    public void ResfreshTotalWeight()
    {
        totalWeights = 0;
        for(int i =0; i< weights.Count ; i++)
        {
            totalWeights += weights[i];
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("SafeZone"))
        {
            for (int i = 0; i < values.Count; i++)
            {
                AudioManager.Instance.PlayLeaveThing();
                Score.Instance.TotalValue += values[i];
                values.Remove(values[i]);
                weights.Remove(weights[i]);
                ResfreshTotalWeight();
            }
        }
    }
}
