using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private bool discardAllObjects;

    [SerializeField] private float speed;
    [SerializeField] private float startWaitTime;
    [SerializeField] private Transform[] patrolPoints;

    private float waitTime;
    private int currentPatrolPoint;

    void Start()
    {
        currentPatrolPoint = Random.Range(0, patrolPoints.Length);
        waitTime = startWaitTime;
    }

    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, patrolPoints[currentPatrolPoint].position, speed * Time.deltaTime);

        if (Vector2.Distance(transform.position, patrolPoints[currentPatrolPoint].position) < 0.2f)
        {
            if (waitTime <= 0)
            {
                currentPatrolPoint = Random.Range(0, patrolPoints.Length);
                waitTime = startWaitTime;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (discardAllObjects || damage >= GiveObject.Instance.values.Count)
            {
                GiveObject.Instance.values.Clear();
                GiveObject.Instance.weights.Clear();
            }
            else
            {
                for(int i=0; i<damage;i++)
                    GiveObject.Instance.values.Remove(GiveObject.Instance.values[i]);

                for (int i = 0; i < damage; i++)
                    GiveObject.Instance.weights.Remove(GiveObject.Instance.weights[i]);
            }
            GiveObject.Instance.ResfreshTotalWeight();
            Destroy(gameObject);
        }
    }
}
