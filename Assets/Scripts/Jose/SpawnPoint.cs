﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    [SerializeField] private List<GameObject> objectsToSpawn;
    [SerializeField] private float timeToStartSpawn;
    [SerializeField] private float timeToRepeatSpawn;

    private void Start()
    {
        InvokeRepeating("InstantiatePickableObject", timeToStartSpawn, timeToRepeatSpawn);
    }

    public void InstantiatePickableObject()
    {
        Instantiate(objectsToSpawn[Random.Range(0, objectsToSpawn.Count)]);
    }
}
