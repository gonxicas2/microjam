﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : Singleton<Score>
{
    [SerializeField]
    private TextMeshProUGUI totalValueT;

    private int totalValue = 0;

    public int TotalValue
    {
        get { return totalValue; }
        set
        {
            totalValue = value; totalValueT.text = "Score: " + totalValue;
        }
    }
}
