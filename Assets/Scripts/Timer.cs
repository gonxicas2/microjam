﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : Singleton<Timer>
{
    public float StartingTime;
    public TMP_Text TimerText;

    private float _remainingTime;
    private bool _timerStarted = false;

    private void Update()
    {
        if (_timerStarted)
        {
            if (_remainingTime >= 0)
            {
                _remainingTime -= Time.deltaTime;
            }
            else
            {
                _remainingTime = 0;
                StopTimer();
            }

            TimerText.text = "Time: " + _remainingTime.ToString("f1");
        }
    }

    public void StartTimer()
    {
        _timerStarted = true;
        _remainingTime = StartingTime;
    }

    public void StopTimer()
    {
        _timerStarted = false;
        GameManager.Instance.EndGame();
    }
}
