﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saco : Singleton<Saco>
{
    private Animator _animator;
    private PlayerController _player;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _player = FindObjectOfType<PlayerController>();
    }
    
    public void SetWalkAnim(bool value) => _animator.SetBool("Walk", value);
    public void SetFallAnim() => _animator.SetTrigger("Fall");
}
