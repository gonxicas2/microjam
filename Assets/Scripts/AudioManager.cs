﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AudioManager : Singleton<AudioManager>
{
    public AudioSource AudioSource;
    public AudioClip TakeThingSound;
    public AudioClip FallSound;
    public AudioClip LeaveThing;

    private void Update()
    {
        AudioSource.pitch = Random.Range(0.8f, 1.2f);
    }

    public void PlayTakeThingSound() => AudioSource.PlayOneShot(TakeThingSound);
    public void PlayFallSound() => AudioSource.PlayOneShot(FallSound);
    public void PlayLeaveThing() => AudioSource.PlayOneShot(LeaveThing);
}
