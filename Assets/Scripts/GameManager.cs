﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : Singleton<GameManager>
{
    public GameObject EndPanel;
    public TMP_Text EndText;

    public bool GameStarted = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if ("MainMenu" == SceneManager.GetActiveScene().name)
            {
                ExitGame();
            }
            else
            {
                LoadScene("MainMenu");
            }
        }
    }

    public void StartGame()
    {
        Timer.Instance.StartTimer();
        GameStarted = true;
    }

    public void EndGame()
    {
        EndPanel.SetActive(true);
        EndText.text = "Has conseguido " + Score.Instance.TotalValue + " puntos";
        GameStarted = false;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}