﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 1;
    public float stunTime = 1;

    private float _horizontal;
    private float _vertical;
    private bool _canMove ;
    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void Start()
    {
        _canMove = true;
    }

    private void Update()
    {
        CheckInputs();

        if (GameManager.Instance.GameStarted == true)
        {
            Move();
        }
    }

    private void CheckInputs()
    {
        _horizontal = Input.GetAxisRaw("Horizontal");
        _vertical = Input.GetAxisRaw("Vertical");
        if(_canMove)
        {
            _animator.SetFloat("XDir", _horizontal);
            _animator.SetFloat("YDir", _vertical);
        }
    }

    private void Move()
    {
        if (_horizontal == 0 && _vertical == 0)
        {
            SetWalkAnim(false);
            return;
        }

        if (!_canMove) return;
        
        
        SetWalkAnim(true);
        
        
        var dire = new Vector3(_horizontal, _vertical, 0).normalized;
        
        transform.Translate(dire*speed / (GiveObject.Instance.totalWeights+1)*Time.deltaTime);
    }

    private IEnumerator Stun()
    {
        SetFallAnim();
        _canMove = false;
        yield return new WaitForSeconds(stunTime);
        _canMove = true;


    }

    private void SetWalkAnim(bool value)
    {
        _animator.SetBool("Walk", value);
        Saco.Instance.SetWalkAnim(value);
    }

    private void SetFallAnim()
    {
        _animator.SetTrigger("Fall");
        Saco.Instance.SetFallAnim();
    }

    private void SetTakeThingAnim() => _animator.SetTrigger("TakeThing");
    private void SetCanWalkTrue() => _canMove = true;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Enemy"))
        {
            AudioManager.Instance.PlayFallSound();
            SetFallAnim();
            StartCoroutine(Stun());
        }
        if (other.CompareTag("Coin"))
        {
            _canMove = false;
            SetTakeThingAnim();
        }
        
    }
    
}